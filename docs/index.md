# Python 재귀: 재귀 함수와 예 <sup>[1](#footnote_1)</sup>

> <font size="3">작은 문제를 해결하기 위해 자신을 호출하는 재귀 함수를 작성 방법을 배운다.</font>

## 목차

1. [들어가며](./recursion.md#intro)
1. [Python에서 재귀가 작동하는 방식](./recursion.md#sec_02)
1. [재귀의 장단점](./recursion.md#sec_03)
1. [Python의 재귀 함수 예](./recursion.md#sec_04)
1. [재귀함수를 작성하는 팁과 요령](./recursion.md#sec_05)
1. [요약](./recursion.md#conclusion)

<a name="footnote_1">1</a>: [Python Tutorial 15 — Python Recursion: Recursive Functions and Examples](https://levelup.gitconnected.com/python-tutorial-15-python-recursion-recursive-functions-and-examples-7ed28edc5873?sk=fdb46bb2fe913bc2755d292f0aecc10b)를 편역한 것이다.
